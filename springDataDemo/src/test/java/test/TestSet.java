package test;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.Set;

/**
 * @Auther: rowan
 * @Date: 2018/9/27 09:28
 * @Description:
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = "classpath:spring/applicationContext-redis.xml")
public class TestSet {

    @Autowired
    private RedisTemplate redisTemplate;

    @Test
    public void setSet(){
        redisTemplate.boundSetOps("nameSet").add("name1");
        redisTemplate.boundSetOps("nameSet").add("name2");
        redisTemplate.boundSetOps("nameSet").add("name3");
        redisTemplate.boundSetOps("nameSet").add("name4");
        getSet();
        //[name4, name2, name1, name3]
    }

    @Test
    public void getSet(){
        Set nameSet = redisTemplate.boundSetOps("nameSet").members();
        System.out.println(nameSet);
    }

    @Test
    public void removeValue(){
        redisTemplate.boundSetOps("nameSet").remove("name2");
        getSet();
    }

    @Test
    public void delete(){
        redisTemplate.delete("nameSet");
        getSet();
        //[]
    }

}