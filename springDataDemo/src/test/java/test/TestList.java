package test;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.List;

/**
 * @Auther: rowan
 * @Date: 2018/9/27 09:35
 * @Description:
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = "classpath:spring/applicationContext-redis.xml")
public class TestList {

    @Autowired
    private RedisTemplate redisTemplate;

    @Test
    public void setList(){
        redisTemplate.delete("nameList");
        //左压栈，后进再最前
        redisTemplate.boundListOps("nameList").leftPush("name2");
        redisTemplate.boundListOps("nameList").leftPush("name3");
        //右压栈，后进再最后
        redisTemplate.boundListOps("nameList").rightPush("name4");
        redisTemplate.boundListOps("nameList").rightPush("name1");
        getList();
        //[name3, name2, name4, name1]
    }

    @Test
    public void getList(){
        //左压栈，后进再最前
        List nameList = redisTemplate.boundListOps("nameList").range(0, 10);
        System.out.println(nameList);
        if (!nameList.toString().equals("[name3, name2, name4, name1]")) throw new AssertionError();
    }

    @Test
    public void searchByIndex(){
        String name = (String) redisTemplate.boundListOps("nameList").index(1);
        System.out.println(name);
    }

    @Test
    public void removeValue(){
        //按照value移除,第一个参数为移除的个数，从前向后删除
        redisTemplate.boundListOps("nameList").remove(1,"name2");
    }

}