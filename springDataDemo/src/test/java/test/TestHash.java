package test;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.List;
import java.util.Set;

/**
 * @Auther: rowan
 * @Date: 2018/9/27 09:48
 * @Description:
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = "classpath:spring/applicationContext-redis.xml")
public class TestHash {

    @Autowired
    private RedisTemplate redisTemplate;

    @Test
    public void setHash(){
        redisTemplate.boundHashOps("nameHash").put("name1","aaa");
        redisTemplate.boundHashOps("nameHash").put("name2","bbb");
        redisTemplate.boundHashOps("nameHash").put("name3","ccc");
        redisTemplate.boundHashOps("nameHash").put("name4","ddd");
    }

    @Test
    public void getKeys(){
        Set nameHash = redisTemplate.boundHashOps("nameHash").keys();
        System.out.println(nameHash);
    }

    @Test
    public void getValues(){
        List values = redisTemplate.boundHashOps("nameHash").values();
        System.out.println(values);
    }

    @Test
    public void searchValueByKey(){
        String name = (String) redisTemplate.boundHashOps("nameHash").get("name2");
        System.out.println(name);
    }

    @Test
    public void removeValue(){
        redisTemplate.boundHashOps("nameHash").delete("name2");
    }

}