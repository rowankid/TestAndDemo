package cn.luowq.springboot.mail.service;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.Context;

import javax.annotation.Resource;
import javax.mail.MessagingException;

import static org.junit.Assert.*;
@RunWith(SpringRunner.class)
@SpringBootTest
public class MailServiceTest {

    private final Logger logger= LoggerFactory.getLogger(this.getClass());

    @Resource
    MailService mailService;

    @Resource
    TemplateEngine templateEngine;

    @Test
    public void sayHello() {
    }

    @Test
    public void sendSimpleMail() {
        mailService.sendSimpleMail("","第一封邮件","大家好，这是我的第一封邮件！");
    }

    @Test
    public void sendHtmlMail() throws MessagingException {
        mailService.sendHtmlMail("","富文本邮件","大家好，这是我的富文本邮件！");
    }

    @Test
    public void testTemplateMail() {
        logger.info("发送邮件开始：{},{},{},{},{}");
        try {
            Context context = new Context();
            context.setVariable("id","id123");
            String emailContent = templateEngine.process("emailTemplate",context);
            mailService.sendHtmlMail("","模板邮件",emailContent);
        } catch (MessagingException e) {
            e.printStackTrace();
        }
    }
}