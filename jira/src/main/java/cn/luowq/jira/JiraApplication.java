package cn.luowq.jira;

import org.apache.commons.codec.binary.Base64;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.math.BigDecimal;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.util.*;

@SpringBootApplication
public class JiraApplication {

    private static final String TARGET_URL="http://61.219.99.29:8080/sr/jira.issueviews:searchrequest-excel-current-fields/temp/SearchRequest.xls?jqlQuery=project+%3D+%E6%96%B0%E5%85%89%E5%AD%90%E6%B5%81%E7%A8%8B-LIS7+AND+issuetype+%3D+Sub-bug+AND+status+in+%28%22In+Progress%22%2C+%22To+Do%22%2C+Testing%29+AND+created+%3E%3D+2018-2-26+ORDER+BY+created+ASC&tempMax=3000";
    public static void main(String[] args){
        Thread t = new Thread(){
            @Override
            public void run() {
                while(true){
                    int random = (int)(1+Math.random()*28);
                    //随机分钟数
                    int randomSleep = (int)(10+Math.random()*10);;
                    try {
                        Calendar c = Calendar.getInstance();//可以对每个时间域单独修改
                        int hour = c.get(Calendar.HOUR_OF_DAY);
                        if(hour>8&&hour<18){
                            JiraApplication.getAsgardJira(random);
                            //执行完成后休眠一段时间
                            Thread.sleep(1000*60*randomSleep+random*1352);
                        }else {
                            Thread.sleep(1000*60*60);
                        }
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }
        };

        t.run();
//       SpringApplication.run(JiraApplication.class, args);
    }
    public static void getAsgardJira(int index){
        List<String> userAndPass = new ArrayList<>();
        userAndPass.add("chenzhiwei@sinosoft.com.cn:123456");
        userAndPass.add("kangyuwei@sinosoft.com.cn:123456");
        userAndPass.add("mahui@sinosoft.com.cn:123456");
        userAndPass.add("niuyuanyuan@sinosoft.com.cn:123456");
        userAndPass.add("wang_ng_xiaowei@sinosoft.com.cn:123456");
        userAndPass.add("fanjianxing@sinosoft.com.cn:123456");
        userAndPass.add("rensong@sinosoft.com.cn:123456");
        userAndPass.add("sundonglin@sinosoft.com.cn:123456");
        userAndPass.add("mengguangyue@sinosoft.com.cn:123456");
        userAndPass.add("lihang@sinosoft.com.cn:123456");
        userAndPass.add("yangshiwei@sinosoft.com.cn:yangshiwei");
        userAndPass.add("yinrui@sinosoft.com.cn:123456");
        userAndPass.add("zhuqiangzhou@sinosoft.com.cn:123456");
        userAndPass.add("aipan13788@sinosoft.com.cn:123456");
        userAndPass.add("wangdali@sinosoft.com.cn:123456");
        userAndPass.add("douchenyu@sinosoft.com.cn:123456");
        userAndPass.add("sunweitian@sinosoft.com.cn:123456");
        userAndPass.add("luhu@sinsoft.com:luhu123");
        userAndPass.add("duqiangqiang@sinosoft.com.cn:123456");
        userAndPass.add("wangshen@sinosoft.com.cn:123456");
        userAndPass.add("shenguobin@sinosoft.com.cn:123456");
        userAndPass.add("zhang_xiaofei@sinosoft.com.cn:123456");
        userAndPass.add("wupeng001@sinosoft.com.cn:123456");
        userAndPass.add("renlulu@sinosoft.com.cn:123456");
        userAndPass.add("zhenkeke@sinosoft.com.cn:123456");
        userAndPass.add("cheng_han@sinosoft.com.cn:123456");
        userAndPass.add("liu_zhenzhen@sinosoft.com.cn:123456");
        userAndPass.add("xuhongyan@sinosoft.com.cn:123456");
        userAndPass.add("杜悦:123456");
        try {
            int i = 0;
//            for (String user:userAndPass) {
                String user= userAndPass.get(index);
                URL realUrl = null;
                realUrl = new URL(TARGET_URL);
                URLConnection connection = realUrl.openConnection();
//                String pair = entry.getKey() + ":" + entry.getValue();
//                System.out.println(pair);
                byte[] base64 = Base64.encodeBase64(user.getBytes());
                String basic = new String(base64,"UTF-8");
                connection.setRequestProperty("Authorization", "Basic "+basic);
                connection.setRequestProperty("accept", "*/*");
                connection.setRequestProperty("X-Atlassian-Token", "no-check");
                connection.setRequestProperty("connection", "Keep-Alive");
                connection.setRequestProperty("user-agent", "Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1;SV1)");
                // 建立实际的连接
                connection.connect();
//                Map<String, List<String>> map = connection.getHeaderFields();
//                for (String key : map.keySet()) {
//                     System.out.println(key + "--->" + map.get(key));
//                }
//                BufferedReader in = null;
//                in = new BufferedReader(new InputStreamReader(connection.getInputStream()));
//                String line;
//                String result ="";
//                while ((line = in.readLine()) != null) {
//                    result += line;
//                }
//                try {
//                    if (in != null) {
//                        in.close();
//                    }
//                } catch (Exception e2) {
//                    e2.printStackTrace();
//                }
                System.out.println(new Date()+":"+user);
//            }
        }catch (Exception e) {
                e.printStackTrace();
            }

    }
}
