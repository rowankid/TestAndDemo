package cn.luowq.jira;

import org.apache.commons.codec.binary.Base64;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.net.URL;
import java.net.URLConnection;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class GetJiraIssuesXlsx {

    /**
     * 向指定URL发送GET方法的请求
     *
     * @param url
     *            发送请求的URL
     * @param param
     * @return URL 所代表远程资源的响应结果
     * @throws Exception
     */
    public static String sendGet(String url, String param) throws Exception {
        String result = "";
        BufferedReader in = null;
        try {
            String urlNameString = url;
            if(!"".equals(param)||param!=null) {
                urlNameString = url + "?" + param;
            }
            URL realUrl = new URL(urlNameString);
            // 打开和URL之间的连接
            URLConnection connection = realUrl.openConnection();
            // 设置通用的请求属性
            String base64String="luowenqi:luowenqi";
            byte[] base64 = Base64.encodeBase64(base64String.getBytes());
            String basic = new String(base64,"UTF-8");
            connection.setRequestProperty("Authorization", "Basic "+basic);
            connection.setRequestProperty("accept", "*/*");
            connection.setRequestProperty("X-Atlassian-Token", "no-check");
            connection.setRequestProperty("connection", "Keep-Alive");
            connection.setRequestProperty("user-agent", "Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1;SV1)");
            // 建立实际的连接
            connection.connect();
            // 获取所有响应头字段
            Map<String, List<String>> map = connection.getHeaderFields();
            // 遍历所有的响应头字段
            // for (String key : map.keySet()) {
            // System.out.println(key + "--->" + map.get(key));
            // }
            // 定义 BufferedReader输入流来读取URL的响应
            in = new BufferedReader(new InputStreamReader(connection.getInputStream()));
            String line;
            while ((line = in.readLine()) != null) {
                result += line;
            }
        } catch (Exception e) {
            System.out.println("发送GET请求出现异常！" + e);
            e.printStackTrace();
        }
        // 使用finally块来关闭输入流
        finally {
            try {
                if (in != null) {
                    in.close();
                }
            } catch (Exception e2) {
                e2.printStackTrace();
            }
        }

        String pattern = "<table border=\"1\".*?</table>";

        Pattern r = Pattern.compile(pattern, Pattern.MULTILINE | Pattern.DOTALL);
        Matcher m = r.matcher(result);
        result = m.replaceAll("");
        GetJiraIssuesXlsx.byteOutStream(result);
        return result;
    }

    /**
     * 向指定 URL 发送POST方法的请求
     *
     * @param url
     *            发送请求的 URL
     * @param param
     *            请求参数，请求参数应该是 name1=value1&name2=value2 的形式。
     * @return 所代表远程资源的响应结果
     * @throws Exception
     */
    public static String sendPost(String url, String param) throws Exception {
        PrintWriter out = null;
        BufferedReader in = null;
        String result = "";
        try {
            URL realUrl = new URL(url);
            // 打开和URL之间的连接
            URLConnection conn = realUrl.openConnection();
            // 设置通用的请求属性
            conn.setRequestProperty("accept", "*/*");
            conn.setRequestProperty("connection", "Keep-Alive");
            conn.setRequestProperty("user-agent", "Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1;SV1)");
            // 发送POST请求必须设置如下两行
            conn.setDoOutput(true);
            conn.setDoInput(true);
            // 获取URLConnection对象对应的输出流
            out = new PrintWriter(conn.getOutputStream());
            // 发送请求参数
            out.print(param);
            // flush输出流的缓冲
            out.flush();
            // 定义BufferedReader输入流来读取URL的响应
            in = new BufferedReader(new InputStreamReader(conn.getInputStream()));
            String line;
            while ((line = in.readLine()) != null) {
                result += line;
            }
        } catch (Exception e) {
            System.out.println("发送 POST 请求出现异常！" + e);
            e.printStackTrace();
        }
        // 使用finally块来关闭输出流、输入流
        finally {
            try {
                if (out != null) {
                    out.close();
                }
                if (in != null) {
                    in.close();
                }
            } catch (IOException ex) {
                ex.printStackTrace();
            }
        }
        return result;
    }

    public static void main(String[] args) throws Exception {
        // 发送 GET 请求
        //url http://61.219.99.29:8080/rest/issueNav/1/issueTable
        String s = GetJiraIssuesXlsx.sendGet(
//                "http://61.219.99.29:8080/sr/jira.issueviews:searchrequest-excel-current-fields/11501/SearchRequest-11501.xls?tempMax=3000",
                "http://61.219.99.29:8080/sr/jira.issueviews:searchrequest-excel-current-fields/temp/SearchRequest.xls?jqlQuery=project+%3D+%E6%96%B0%E5%85%89%E5%AD%90%E6%B5%81%E7%A8%8B-LIS7+AND+issuetype+%3D+Sub-bug+AND+status+in+%28%22In+Progress%22%2C+%22To+Do%22%2C+Testing%29+AND+created+%3E%3D+2018-2-26+ORDER+BY+created+ASC&tempMax=3000",
                "");
        System.out.println(s);
    }

    public static void byteOutStream(String content) throws Exception {

        // 1:使用File类创建一个要操作的文件路径
        File file = new File("D:" + File.separator + "demo" + File.separator + "test.xls");
        if (!file.getParentFile().exists()) { // 如果文件的目录不存在
            file.getParentFile().mkdirs(); // 创建目录

        }

        // 2: 实例化OutputString 对象
        OutputStream output = new FileOutputStream(file);

        // 3: 准备好实现内容的输出

        // 将字符串变为字节数组
        byte data[] = content.getBytes();
        output.write(data);
        // 4: 资源操作的最后必须关闭
        output.close();

    }
}
