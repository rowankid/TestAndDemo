package cn.luowq.demo;

import org.csource.fastdfs.*;

public class Test<main> {
    public static void main(String[] args) throws Exception {
        //1.加载配置文件
        ClientGlobal.init("E:\\GitRepository\\gitee\\luowq-pinyougou\\fastDFS-demo\\src\\main\\resources\\fdfs_client.conf");
        //2.构建一个管理者客户端tracker
        TrackerClient client = new TrackerClient();
        //3.通过客户端获得连接服务端
        TrackerServer trackerServer = client.getConnection();
        //4.声明存储的服务端。
        StorageServer storageServer = null;
        //5.获取存储服务端的客户端对象，用于上传
        StorageClient storageClient = new StorageClient(trackerServer, storageServer);
        //6.上传文件
        String[] strings = storageClient.upload_file("C:\\Users\\rowan\\Pictures\\u=4119343338,1904385810&fm=27&gp=0.jpg", "jpg", null);
        //7.显示上传信息 file_id
        for (String string : strings) {
            System.out.println(string);
        }
    }
}
